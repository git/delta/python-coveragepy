.. Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
.. For details: https://bitbucket.org/ned/coveragepy/src/default/NOTICE.txt

===========
Coverage.py
===========

Development of coverage.py has moved to `GitHub`_.

.. _GitHub: https://github.com/nedbat/coveragepy
